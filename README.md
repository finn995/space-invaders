Space invaders game
===================

The final project from How to Code: Simple Data.

You can download the executable [*here*](http://www.mediafire.com/file/z6lj70f2im8thoq/space-invaders-executable.zip).

Link to the course website: [*UBCx-HtC1x*](https://www.edx.org/course/how-code-simple-data-ubcx-htc1x).